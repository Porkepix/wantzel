# encoding: utf-8
"""
Testing commands of wantzel
"""

from mock import Mock
import unittest

import tests.config
from messages import messages
from wantzel import DEBUG, get_cursor, Utils, Wantzel

LOG_FILE = "wantzel_test.log"
LOG_LEVEL = DEBUG


def fake_send_message(channel, message):
    global global_channel, global_message
    global_channel = channel
    global_message = message

def fake_topic(topic):
    global global_topic
    global_topic = topic


class TestWantzel(unittest.TestCase):
    def setUp(self):
        self.wantzel = Wantzel()
        # Faking send_message on IRC
        self.wantzel.send_message = Mock()
        # Faking topic change on IRC
        self.wantzel.irc = Mock()
        self.wantzel.irc.client = Mock()
        self.wantzel.irc.client.topic = Mock()
        cursor = get_cursor()
        # Creating tables if necessary
        with open("create_mysql_tables.sql") as file_handle:
            cursor.execute(file_handle.read())
        # clearing datas
        cursor.execute("DELETE FROM presse")

    def tearDown(self):
        # Destroying database
        cursor = get_cursor()
        cursor.execute("DELETE FROM presse")

    def test_command_help(self):
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~help")
        self.wantzel.send_message.assert_called_once_with("#test_channel", messages["help"])

    def test_command_help_command(self):
        commands = ['help', 'rp', 'status', 'stats', 'kill', 'admin']
        for command in commands:
            self.wantzel.on_privmsg("test!test.me", "#test_channel", "~help %s" % command)
            self.wantzel.send_message.assert_called_with(
                "#test_channel",
                messages["help_%s" % command]
            )

    def test_command_rp_with_no_url(self):
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~rp nothing")
        self.wantzel.send_message.assert_not_called()
        cursor = get_cursor()
        cursor.execute("SELECT COUNT(*) FROM presse")
        rows = cursor.fetchall()
        self.assertEqual(0, int(rows[0][0]))

    def test_command_rp_new_article(self):
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~rp http://test.me/article")
        self.wantzel.send_message.assert_called_once_with(
            "#test_channel",
            messages["rp_new_article"] % "test"
        )
        cursor = get_cursor()
        cursor.execute("SELECT COUNT(*) FROM presse")
        rows = cursor.fetchall()
        self.assertEqual(1, int(rows[0][0]))
        cursor.execute("SELECT note FROM presse WHERE url='%s'" % "http://test.me/article")
        rows = cursor.fetchall()
        self.assertEqual(1, int(rows[0][0]))

    def test_command_rp_known_article(self):
        cursor = get_cursor()
        # Adding article for first time
        self.wantzel.on_privmsg("first_test!test.me", "#test_channel", "~rp http://test.me/article")
        # Then, just add a point
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~rp http://test.me/article")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["rp_known_article"] % "test"
        )
        cursor.execute("SELECT COUNT(*) FROM presse")
        rows = cursor.fetchall()
        self.assertEqual(1, int(rows[0][0]))
        cursor.execute("SELECT note FROM presse WHERE url='%s'" % "http://test.me/article")
        rows = cursor.fetchall()
        self.assertEqual(2, int(rows[0][0]))

    def test_command_rp_taken_article(self):
        cursor = get_cursor()
        # Adding article for first time
        self.wantzel.on_privmsg("first_test!test.me", "#test_channel", "~rp http://test.me/article")
        self.wantzel.on_privmsg("second_test!test.me", "#test_channel", "~rp http://test.me/article")
        # Then, add a third point
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~rp http://test.me/article")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["rp_taken_article"] % "test"
        )
        self.wantzel.irc.client.topic.assert_called_once_with("#lqdn-rp", messages["topic"] % 1)
        cursor.execute("SELECT COUNT(*) FROM presse")
        rows = cursor.fetchall()
        self.assertEqual(1, int(rows[0][0]))
        cursor.execute("SELECT note FROM presse WHERE url='%s'" % "http://test.me/article")
        rows = cursor.fetchall()
        self.assertEqual(3, int(rows[0][0]))

